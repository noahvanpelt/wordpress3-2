<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Sydney
 */

get_header(); ?>

<div class="front-page">
  <div class="news-wrapper">
    <div class="container">
      <h5>NEWS</h5>
      <ul>
        <?php $args = array(
            'numberposts' => 3,                //表示（取得）する記事の数
            'post_type' => ''    //投稿タイプの指定
          );
          $posts = get_posts( $args );
          if(  $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
          <?php endforeach; ?>
        <?php else : //記事が無い場合 ?>
          <li><p>記事はまだありません。</p></li>
        <?php endif;
        wp_reset_postdata(); //クエリのリセット ?>
      </ul>
    </div>
  </div>



  <div class="liveinfo-wrapper">
    <div class="container">
      <h5>LIVE INFO</h5>
      <ul>
        <?php $args = array(
            'numberposts' => 3,                //表示（取得）する記事の数
            'post_type' => 'liveinfo'    //投稿タイプの指定
          );
          $posts = get_posts( $args );
          if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endforeach; ?>
        <?php else : //記事が無い場合 ?>
        <li><p>記事はまだありません。</p></li>
        <?php endif;
        wp_reset_postdata(); //クエリのリセット ?>
      </ul>
    </div>
  </div>

  <div class="sns-wrapper">
    <div class="container">
      <h5>SNS</h5>

    </div>
  </div>

  <div class="footmenu-wrapper">
    <div class="overlay">
      <div class="container">
        <h5></h5>
      </div>
    </div>
  </div>



<!--
<script>
  var _window = $(window),
      _header = $('.site-header'),
      heroBottom;

      _window.on('scroll',function(){
        heroBottom = $('.site-header').height();
    if(_window.scrollTop() > heroBottom){
        _header.addClass('fixed');
    }
    else{
        _header.removeClass('fixed');
    }
});

_window.trigger('scroll');

</script>

-->
</div>



<?php get_footer(); ?>
