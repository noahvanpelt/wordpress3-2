=== Spotify Play Button for WordPress ===
Contributors: jonkastonka
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9344931
Tags: spotify, playlist, spotify play button, spotify play button for wordpress
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 1.34

Spotify Play Button can easily be added to post or page for instant play of album, playlist or song.

Oh, and it has an admin page in WordPress too.

== Description ==

Spotify Play Button easily added to post or page for instant play of album, playlist or song.

Album example: [spotifyplaybutton play="spotify:album:7JggdVIipgSShK1uk7N1hP"]

Playlist example: [spotifyplaybutton play="spotify:user:jonk:playlist:65ujzBs6WTdWDIr17dOXUm"]

Song example: [spotifyplaybutton play="spotify:track:2qntSA2cwerjTduHPuKnW5"]

You don't have to remember these shortcodes since there is a button in the admin page that let's you add a Spotify Play Button directly.

Simply right click on album, playlist or song in Spotify and click "Share" and then click "URI". Either paste that together with the shortcode above or just use the admin button and paste the URI there.

You can set the style for your Spotify Play Buttons on the "Spotify Play Button Settings" page under the "Settings" menu (http://YOURBLOG/wp-admin/options-general.php?page=spotifyplaybutton_settings).

You can also add attributes to customize a single Spotify Play Button:

1. view

2. size

3. sizetype

4. theme

5. link

All of these will override the settings in "Spotify Play Button Settings" for the Spotify Play Button and they are all optional.

Example:
[spotifyplaybutton play="spotify:user:jonk:playlist:65ujzBs6WTdWDIr17dOXUm" view="coverart" size="0" sizetype="big" theme="white"]

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the folder "spotify-play-button-for-wordpress" to the "/wp-content/plugins/" directory

2. Activate the plugin through the "Plugins" menu in WordPress

3. Go to "Spotify Play Button Settings" under the "Settings" menu (http://YOURBLOG/wp-admin/options-general.php?page=spotifyplaybutton_settings) to make your default settings for your Spotify Play Buttons.

4. Start adding your Spotify Play Buttons!

== Frequently Asked Questions ==

None, yet.

== Screenshots ==

1. Editing your post
2. Adding the Spotify URI
3. The nice result
4. The settings page

== Changelog ==

= 1.34 =
Typeo

= 1.33 =
* Better looking UI.
* Bugfixes
* Cleanup

= 1.3 =
Added button to TinyMCE to make it easier to add lists to pages and posts without having to remember the shortcode.

= 1.2 =
* Updated with the news specs from Spotify
* Responsive mode added
* Simplified admin page

= 1.1 =
Added link for opening the lista in Spotify, you can disable it in the settings

= 1.02 =
Cleanup

= 1.01.b =
Requires and Tested versions.

= 1.01.a =
Touch to make you know it still works fine. :)

= 1.01 =
* Getting assets right, editing readme

= 1.00 =
* I feel confident!
