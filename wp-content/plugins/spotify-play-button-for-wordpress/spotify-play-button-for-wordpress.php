<?php
/*
Plugin Name: Spotify Play Button for WordPress
Plugin URI: https://wordpress.org/plugins/spotify-play-button-for-wordpress/
Description: Show Spotify Play Button in any page or post with a simple hook. Example: [spotifyplaybutton play="spotify:user:jonk:playlist:65ujzBs6WTdWDIr17dOXUm" view="list" size="0" sizetype="small" theme="black"]. The best way to enter the value for the play-attribute is to go to your playlist, right click and choose "Copy Spotify URI" and paste it in the Text-view since you otherwise get a link and not the URI pasted. The plugin also has a general settings page in wp-admin to manage your default settings.
Version: 1.34
Author: jonkastonka
Author URI: http://jonk.pirateboy.net
Text Domain: spotifyplaybutton_text
*/

$spotifyplaybutton_path = plugin_dir_url( __FILE__ );

function spotifyplaybutton_load_textdomain() {
	load_plugin_textdomain( 'spotifyplaybutton_text', false, WP_LANG_DIR ); 
}
add_action( 'plugins_loaded', 'spotifyplaybutton_load_textdomain' ); //plugins_loaded

function spotifyplaybutton_menu() {
	add_options_page('Spotify Play Button settings', 'Spotify Play Button settings', 'manage_options', 'spotifyplaybutton_settings', 'spotifyplaybutton_options');
}
add_action('admin_menu', 'spotifyplaybutton_menu');

function spotifyplaybutton_options() {
	global $spotifyplaybutton_Order;
	global $spotifyplaybutton_OrderType;

	if( isset($_POST['save_spotifyplaybutton_settings'] )) {

        update_option('spotifyplaybutton_view', $_POST['spotifyplaybutton_view'] );
        update_option('spotifyplaybutton_size', $_POST['spotifyplaybutton_size'] );
        update_option('spotifyplaybutton_sizetype', $_POST['spotifyplaybutton_sizetype'] );
        update_option('spotifyplaybutton_theme', $_POST['spotifyplaybutton_theme'] );
        update_option('spotifyplaybutton_link', $_POST['spotifyplaybutton_link'] );

		echo "<div id=\"message\" class=\"updated fade\"><p>Your settings are now updated</p></div>\n";
		
	}
	$spotifyplaybutton_view = stripslashes( get_option( 'spotifyplaybutton_view' ) );
	$spotifyplaybutton_size = stripslashes( get_option( 'spotifyplaybutton_size' ) );
	$spotifyplaybutton_sizetype = stripslashes( get_option( 'spotifyplaybutton_sizetype' ) );
	$spotifyplaybutton_theme = stripslashes( get_option( 'spotifyplaybutton_theme' ) );	
	$spotifyplaybutton_link = stripslashes( get_option( 'spotifyplaybutton_link' ) );	
	?>
  <div class="wrap">
	<h2>Spotify Play Button settings</h2>
	<form method="post">
		<table class="form-table">
			<tr valign="top">
				<th scope="row">View</th>
				<td>
					<select style="width:200px;" name="spotifyplaybutton_view" id="spotifyplaybutton_view">
						<option <?php if ($spotifyplaybutton_view == 'list') { echo 'selected="selected"'; } ?> value="">List</option>
						<option <?php if ($spotifyplaybutton_view == 'coverart') { echo 'selected="selected"'; } ?> value="coverart">Coverart</option>
					</select>
					<br/><span style="font-style:italic;">The button can show a list of songs or the cover art of each song.</span>
				</td> 
			</tr>
			<tr valign="top">
				<th scope="row">Max width</th>
				<td>
					<input type="text" style="width:200px;" name="spotifyplaybutton_size" id="spotifyplaybutton_size" value="<?php if (isset($spotifyplaybutton_size) && $spotifyplaybutton_size != '') { print($spotifyplaybutton_size); } else { print('0'); } ?>"/>
					<br/><span style="font-style:italic;">Example: 500, enter 0 for full responsive mode, ie 100% width (recommended)</span>
				</td> 
			</tr>
			<tr valign="top">
				<th scope="row">Size type</th>
				<td>
					<select style="width:200px;" name="spotifyplaybutton_sizetype" id="spotifyplaybutton_sizetype">
						<option <?php if ($spotifyplaybutton_sizetype == 'big') { echo 'selected="selected"'; } ?> value="big">Big</option>
						<option <?php if ($spotifyplaybutton_sizetype == 'compact') { echo 'selected="selected"'; } ?> value="compact">Compact</option>
					</select>
					<br/><span style="font-style:italic;">Big has the playlist or coverart visible. Compact shows the current song.</span>
				</td> 
			</tr>
			<tr valign="top">
				<th scope="row">Theme</th>
				<td>
					<select style="width:200px;" name="spotifyplaybutton_theme" id="spotifyplaybutton_theme">
						<option <?php if ($spotifyplaybutton_theme == 'black') { echo 'selected="selected"'; } ?> value="">Dark</option>
						<option <?php if ($spotifyplaybutton_theme == 'white') { echo 'selected="selected"'; } ?> value="white">White</option>
					</select>
					<br/><span style="font-style:italic;">The look of the song list.</span>
				</td> 
			</tr>
			<tr valign="top">
				<th scope="row">Link</th>
				<td>
					<select style="width:200px;" name="spotifyplaybutton_link" id="spotifyplaybutton_link">
						<option <?php if ($spotifyplaybutton_link == 'yes') { echo 'selected="selected"'; } ?> value="">Yes</option>
						<option <?php if ($spotifyplaybutton_link == 'no') { echo 'selected="selected"'; } ?> value="no">No</option>
					</select>
					<br/><span style="font-style:italic;">Shows a link to the playlist below</span>
				</td> 
			</tr>
		</table>

		<div class="submit">
			<input type="submit" name="save_spotifyplaybutton_settings" value="<?php _e('Save Settings') ?>" class="button-primary" />
		</div>		
	</form>
  </div>
<?php
}

function spotifyplaybutton_func( $atts ) {
	extract( shortcode_atts( array(
		'play' => 'spotify:user:jonk:playlist:2ImDreMyt1Py2iXKtmEStW',
		'view' => get_option('spotifyplaybutton_view','list'),
		'size' => get_option('spotifyplaybutton_size',0),
		'sizetype' => get_option('spotifyplaybutton_sizetype','big'),
		'theme' => get_option('spotifyplaybutton_theme','black')
	), $atts ) );
	
	$size = round($size);
	if ($size == 0) {
		$width = '100%';
	}
	if ($sizetype == "compact") {
		$height = 80;
	} else {
		if ($size == 0) {
			$height = '500';
		} else {
			$height = $size+80;
		}
	}
	$open_spotify_link = '';
	if ( get_option( 'spotifyplaybutton_link','' ) != 'no' ) {
		$url = str_replace( ":", "/", $play );
		$url = str_replace( "spotify/", "https://open.spotify.com/", $url );
		$open_spotify_link = "<p><a href=\"" . $url . "\" target=\"_blank\">" . __("Open in Spotify", "spotifyplaybutton_text") . "</a></p>";
	}
	return "<iframe src=\"https://open.spotify.com/embed?uri={$play}&view={$view}&theme={$theme}\" width=\"{$width}\" height=\"{$height}\" frameborder=\"0\" allowTransparency=\"true\"></iframe>" . $open_spotify_link;
}
add_shortcode( 'spotifyplaybutton', 'spotifyplaybutton_func' );

function spotifyplaybutton_tinymce_button($context){
return $context.=__("
	<a href=\"#TB_inline?&inlineId=spotifyplaybutton_tinymce_popup&width=600&height=550\" class=\"button thickbox\" id=\"spotifyplaybutton_tinymce_popup_button\" title=\"Add Spotify Play Button\"><img src=\"" . plugin_dir_url( __FILE__ ) . "spotify-play-button-for-wordpress-icon.png\" alt=\"Add Spotify Play Button\" style=\"width:auto;height:16px;vertical-align:text-top;display:inline-block;margin:0;\"></a>");
}
add_action('media_buttons_context', 'spotifyplaybutton_tinymce_button');

function spotifyplaybutton_tinymce_popup(){ 
?>
<div id="spotifyplaybutton_tinymce_popup" style="display:none;">
	<div class="wrap" style="position:absolute;">
		<div>
			<div class="my_shortcode_add">
				<p>
					<input type="text" id="spotifyplaybutton_tinymce_popup_playlist" placeholder="<?php _e("Spotify URI", "spotifyplaybutton_text"); ?> *" style="width:100%;">
				</p>
				<p>
					<button class="button-primary" id="spotifyplaybutton_tinymce_popup_insert_button"><?php _e("Insert the Spotify Play Button", "spotifyplaybutton_text"); ?></button>
				</p>
				<p>
					* <?php _e("Right-click on a playlist or album in Spotify, choose Share, click URI and then paste the result in the box above. Finish by clicking the button.", "spotifyplaybutton_text"); ?>
				</p>
				<hr>
				<p>
					<a href="<?php menu_page_url( 'spotifyplaybutton_settings', true ); ?>"><?php _e("Edit general settings for Spotify Play Button", "spotifyplaybutton_text"); ?></a> <?php _e("or add parameters to your shortcode if you want to make this one special.", "spotifyplaybutton_text"); ?>
				</p>
			</div>
		</div>
	</div>
</div>
<?php
}
add_action('admin_footer', 'spotifyplaybutton_tinymce_popup');

function my_shortcode_add_shortcode_to_editor(){?>
<script>
jQuery('#spotifyplaybutton_tinymce_popup_insert_button').on('click',function(){
	var user_content = jQuery('#spotifyplaybutton_tinymce_popup_playlist').val();
	var shortcode = '[spotifyplaybutton play="'+user_content+'"/]';
	if( !tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
		jQuery('textarea#content').val(shortcode);
	} else {
		tinyMCE.execCommand('mceInsertContent', false, shortcode);
	}
	self.parent.tb_remove();
});
</script>
<?php
}
add_action('admin_footer','my_shortcode_add_shortcode_to_editor');
?>
