<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'wordpress3' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'root' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'jZeK4YK%8.2{X]DA}9IU=Ww8<#bwq][_b1IFu3f qpw/G}!9KqDM]N:m=2m^+If,' );
define( 'SECURE_AUTH_KEY',  'g?I5O1$`H6kS:y]a*UW%dl7CeEU&[m?/}/kU5fPcR_xh7L,%bNmEZI}Dg-fomeWj' );
define( 'LOGGED_IN_KEY',    'xJB+17umC69z*7#J`YM9?r0m!SdzLm:3),I4aNl[(9v/?3dX]~<uiUZ8C|^_`dKG' );
define( 'NONCE_KEY',        'vt23S=l3@%Cm|v*-Q vLdl`W^gl|{oz3*09.%=9{`nup/rF%*@z8tUd2{t<rv5#e' );
define( 'AUTH_SALT',        '3.O0AVR 6X%~s=:[lHjFcw/wuS*|&YygW:pf%6mGs%|Q}T(6hajv]@]oUvB1pS:J' );
define( 'SECURE_AUTH_SALT', '=!OT]2F<xi2)Yq>IX.2XAqyq.!hoQE8F@w? Ufj;%8RTH!=14,^Qn3yAk1:5s(sR' );
define( 'LOGGED_IN_SALT',   '#l_2MVW_W^r+0znt^TeqSxWVXMZ%~];![:,+_&YU[3XkUVdf;<xiy|DevTQ3/pTy' );
define( 'NONCE_SALT',       '6*IZ+f>X&9b<|+t0r?qv)fBP^ObQk#|Fh]h3N1}y$.&Uar4:2>a0Im2M,q<}?::v' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', true);

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
